#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "load_page.h"
#include "toml_handler.h"
#include "log.c/src/log.h"

char successHeader[] = "HTTP/1.1 200 OK\nContent-Type: %s\nContent-Length: %lu\n\n";
char notFoundHeader[] = "HTTP/1.1 404 Not Found\nContent-Type: %s\nContent-Length: %lu\n\n";
char imageSuccessHeader[] = "HTTP/1.1 200 OK\n Connection: Keep-Alive\nKeep-Alive: timeout=5, max=1000\nContent-Type: %s\nContent-Length: %lu\nContent-Transfer-Encoding: binary\n\n";

int readFileToBuffer(char *path, char **buffer, long *length) {
    FILE *ftpr;

    if ((ftpr = fopen(path, "rb")) == NULL) {
        perror("file read error");

        return -1;
    }

    fseek(ftpr, 0, SEEK_END);
    *length = ftell(ftpr);
    fseek(ftpr, 0, SEEK_SET);

    *buffer = malloc(*length);
    fread(*buffer, 1, *length, ftpr);
    fclose(ftpr);

    return 0;
}

int convertEndpointToFilePath(char *request, char **filePath) {
    char *requestBuffer = malloc(strlen(request) + 1);
    strcpy(requestBuffer, request);

    char *endpoint;
    
    endpoint = strtok(requestBuffer, " ");
    endpoint = strtok(NULL, " ");

    if ((getRoute(endpoint, filePath)) == -1) {
        *filePath = malloc(strlen(endpoint) + 7);
        strcpy(*filePath, "./html");
        strcat(*filePath, endpoint);
    }

    free(requestBuffer);
}

void constructHeaders(long length, int code, char *contentType, char **headers) {
    if (strncmp(contentType, "image", 5) == 0) {
        sprintf(*headers, imageSuccessHeader, contentType, length);
    } else if (code == 0) {
        sprintf(*headers, successHeader, contentType, length);
    } else if (code == -1) {
        sprintf(*headers, notFoundHeader, contentType, length);
    }
}

void constructResponse(char *headers, char *buffer, long length, char **res) { 
    *res = malloc(length + strlen(headers) + 1);

    strcpy(*res, headers);
    strncat(*res, buffer, length);
}

void getAcceptType(char *request, char **acceptType) {
    char *requestBuffer = malloc(strlen(request) + 1);
    strcpy(requestBuffer, request);

    char *lineToken = strtok(requestBuffer, "\r\n");
    
    while (lineToken != NULL) {
        if (strncmp(lineToken, "Accept:", 7) == 0) {
            char *spaceToken = strtok(lineToken, " ");
            spaceToken = strtok(NULL, " ");

            strcpy(*acceptType, strtok(spaceToken, ","));

            break;
        }

        lineToken = strtok(NULL, "\r\n");
    }

    free(requestBuffer);
}

void loadContent(char *request, char **res, char **fileBuffer) {
    char *filePath;

    convertEndpointToFilePath(request, &filePath);

    char *buffer;
    long length;
    int code;
    if ((code = readFileToBuffer(filePath, &buffer, &length)) == -1) {
        char *notFoundFilePath = malloc(128);
        getNotFound(&notFoundFilePath);
        readFileToBuffer(notFoundFilePath, &buffer, &length);

        free(notFoundFilePath);
    }

    char *acceptType = malloc(1024);
    getAcceptType(request, &acceptType);

    log_debug(acceptType);

    char *headers = malloc(1024);
    constructHeaders(length, code, acceptType, &headers);

    if (strncmp(acceptType, "image", 5) == 0) {
        log_debug("Yes");
        log_debug("%ld", strlen(headers));
        *res = malloc(strlen(headers));
        strcpy(*res, headers);

        *fileBuffer = malloc(length);
        memcpy(*fileBuffer, buffer, length);
    } else {
        constructResponse(headers, buffer, length, res);

        *fileBuffer = NULL;
    }

    free(acceptType);
    free(headers);
    free(buffer);
    free(filePath);
}