#include <unistd.h> 
#include <stdio.h> 
#include <string.h>
#include <stdlib.h> 
#include <sys/socket.h> 
#include <netinet/in.h>

#include "net.h"
#include "thread_handler.h"
#include "log.c/src/log.h"

int setupSocketAndListenAndHandleRequests(int port) {
    log_info("Attempting to open socket on port %d...", port);

    log_info("Attempting to create socket...");

    int server_fd;

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    log_info("Socket creation sucessful");

    log_info("Attempting to set socket options...");

    int opt = 1;

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }

    log_info("Socket options successfully set");

    log_info("Attempting to bind socket to address and port...");

    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    if (bind(server_fd, (struct sockaddr *) &address, sizeof(address)) < 0) { 
        perror("bind failed"); 
        exit(EXIT_FAILURE); 
    }

    log_info("Socket successfully binded");

    log_info("Attempting to start socket listening...");

    if (listen(server_fd, 3) < 0) { 
        perror("listen failed");
        exit(EXIT_FAILURE); 
    }

    log_info("Socket successfully listening on port %d", port);

    int addr_len = sizeof(address);
    int new_socket;

    while (1) {
        if ((new_socket = accept(server_fd, (struct sockaddr *) &address, (socklen_t *) &addr_len)) < 0) {
            log_error("Failed to accept request");
        }

        createNewThreadToHandleRequest(new_socket);
    }

    return 0;
}