#ifndef TOML_HANDLER_H
#define TOML_HANDLER_H

int getRoute(char *endpoint, char **filePath);
int getNotFound(char **filePath);

#endif