#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <pthread.h>

#include "thread_handler.h"
#include "load_page.h"
#include "log.c/src/log.h"

void *handleRequest(void *vargp) {
    int socket = *((int *) vargp);

    int valread;
    char buffer[2048] = {0};

    valread = read(socket, buffer, 2048);

    time_t now = time(NULL);
    struct tm *timeStruct = localtime(&now);

    log_info("Recieved request at %s Buffer:\n %s", asctime(timeStruct), buffer);

    log_info("Attempting to load content...");

    char *res;
    char *fileBuffer;

    loadContent(buffer, &res, &fileBuffer);

    log_info("Content loaded");

    log_info("Sending response...");

    if (fileBuffer != NULL) {
        log_debug("Yes");
        send(socket, res, strlen(res), 0);
        log_debug(res);
        send(socket, fileBuffer, 1594447, 0); 
    } else {
        send(socket, res, strlen(res), 0); 
    }

    log_info("Response sent");

    close(socket);

    log_info("Socket closed");

    free(res);

    pthread_exit(NULL);
}

int createNewThreadToHandleRequest(int socket) {
    pthread_t thread_id;

    log_info("Attempting to create a new thread for request...");

    pthread_create(&thread_id, NULL, handleRequest, (void *) &socket);

    log_info("Thread creation successful");

    log_info("Attempting to join to thread...");

    pthread_join(thread_id, NULL);

    log_info("Thread joining successful");
}