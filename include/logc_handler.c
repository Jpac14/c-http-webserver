#include <stdlib.h>
#include <time.h>

#include "logc_handler.h"
#include "log.c/src/log.h"

int initLogger() {
    time_t now = time(NULL);
    struct tm *timeStruct = localtime(&now);

    char timeStr[128];
    strftime(timeStr, 100, "%F-%H-%M-%S", timeStruct);

    char filePath[256];
    sprintf(filePath, "logs/%s.log", timeStr);

    FILE *fptr;
    fptr = fopen(filePath, "w");

    log_add_fp(fptr, 0);

    log_info("Created log file %s", filePath);
    log_info("Initalised logger");

    return 0;
}