#include <stdio.h>
#include <string.h>

#include "tomlc99/toml.h"

int loadConfigFile(toml_table_t **conf) {
    char errbuf[128];
    FILE *fp = fopen("config.toml", "r");

    *conf = toml_parse_file(fp, errbuf, sizeof(errbuf));

    fclose(fp);

    return 0;
}

int getRoute(char *endpoint, char **filePath) {
    toml_table_t *conf;
    loadConfigFile(&conf);

    toml_table_t *routes;
    routes = toml_table_in(conf, "routes");

    toml_raw_t raw;

    char *endpoint_chopped = endpoint + 1;
    char *key;

    if (strcmp(endpoint_chopped, "") == 0) {
        key = "root";
    } else {
        key = endpoint_chopped;
    }

    if ((raw = toml_raw_in(routes, key)) == 0) {
        return -1;
    }
    toml_rtos(raw, filePath);

    toml_free(conf);

    return 0;
}

int getNotFound(char **filePath) {
    toml_table_t *conf;
    loadConfigFile(&conf);

    toml_raw_t raw;
    raw = toml_raw_in(conf, "not_found");
    toml_rtos(raw, filePath);

    toml_free(conf);

    return 0;
}