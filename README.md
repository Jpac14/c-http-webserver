# C HTTP Server

A semi-functional HTTP Server implemented in C. Only supports files (e.g. HTML, CSS, etc), doesn't support image, video or most HTTP Headers. This code is spaghetti code, if I had to redesign it, I would do it very differently. I would advise against using this, so use at your own peril.
