#include <unistd.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h>
#include <time.h>

int readFileToBuffer(char *path, char **buffer, long *length) {
    FILE *ftpr;

    if ((ftpr = fopen(path, "rb")) == NULL) {
        perror("file read error");

        return -1;
    }

    fseek(ftpr, 0, SEEK_END);
    *length = ftell(ftpr);
    fseek(ftpr, 0, SEEK_SET);

    *buffer = malloc(*length);
    fread(*buffer, 1, *length, ftpr);
    fclose(ftpr);

    return 0;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Too many or too little arguments. Usage: %s <port>\n", argv[0]);

        exit(EXIT_FAILURE);
        return 0;
    }

    int port = atoi(argv[1]);

    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    if (bind(server_fd, (struct sockaddr *) &address, sizeof(address)) < 0) { 
        perror("bind failed"); 
        exit(EXIT_FAILURE); 
    } 

    if (listen(server_fd, 3) < 0) { 
        perror("listen failed"); 
        exit(EXIT_FAILURE); 
    }

    while (1) {
        if ((new_socket = accept(server_fd, (struct sockaddr *) &address, (socklen_t *) &addrlen)) < 0) {
            perror("accept");
        }

        int valread;
        char buffer[2048] = {0};

        valread = read(new_socket, buffer, 2048);

        printf("Request recieved at %ld\n%s\n", time(0), buffer);

        long length;
        char *fileBuffer;

        readFileToBuffer("./html/img.png", &fileBuffer, &length);

        char headers[] = "HTTP/1.1 200 OK\nContent-Type: image/png\nContent-Length: 1594447\nContent-Transfer-Encoding: binary\n\n";

        char *res = malloc(length + strlen(headers)+1);
        strcpy(res, headers);
        memcpy(res+strlen(headers), fileBuffer, length);
        res[length + strlen(headers)+1] = '\0';
        send(new_socket, res, strlen(res), 0);

        close(new_socket);
    }
    
    return 0;
}