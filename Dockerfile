FROM ubuntu:18.04

RUN mkdir /c-http-webserver

WORKDIR /c-http-webserver

RUN mkdir html

COPY webserver .
COPY html/ html/

CMD ["./webserver", "8080"]