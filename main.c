#include <stdlib.h> 
#include <stdio.h> 

#include "include/net.h"
#include "include/logc_handler.h"
#include "include/log.c/src/log.h"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Too many or too little arguments. Usage: %s <port>\n", argv[0]);

        exit(EXIT_FAILURE);
        return 0;
    }

    initLogger();

    log_info("\n===========================================\n"
             "            Starting JWEB v1.0\n"
             "            Developed by Jpac14\n"
             "https://gitlab.com/Jpac14/c-http-webserver/\n"
             "===========================================\n");

    int port = atoi(argv[1]);

    setupSocketAndListenAndHandleRequests(port);
}