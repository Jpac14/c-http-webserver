CC = gcc
CCFLAGS = -pthread -g
SRCS = main.c include/load_page.c include/thread_handler.c include/net.c include/toml_handler.c include/logc_handler.c
TOMLC_ARGS = include/tomlc99/toml.c -DLOG_USE_COLOR
LOGC_ARGS = include/log.c/src/log.c
BIN = webserver
LOGS = logs/*.log
PORT = 8080

.PHONY: compilerun
compilerun: compile run

.PHONY: compile
compile:
	@echo "Compiling..."
	@$(CC) $(SRCS) $(TOMLC_ARGS) $(LOGC_ARGS) -o $(BIN) $(CCFLAGS)

.PHONY: run
run:
	@echo "Running..."
	@./$(BIN) $(PORT)

.PHONY: clean
clean:
	@echo "Cleaning up..."
	@rm $(BIN) $(LOGS)
